﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Person
    {
        [Key]
        public int PersonNumber { get; set; }
        
        public string Name { get; set; }
        public int Age { get; set; }
    }
}