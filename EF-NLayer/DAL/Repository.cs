﻿using System.Collections.Generic;
using System.Linq;
using Domain;

namespace DAL
{
    public class Repository : IRepository
    {
        private readonly AppDbContext _ctx;

        public Repository(AppDbContext ctx)
        {
            _ctx = ctx;
        }

        public IEnumerable<Person> ReadAllPeople(string name = null, int? age = null)
        {
            IQueryable<Person> result = _ctx.People;
            
            if (name != null)
                result = result.Where(p => p.Name.Contains(name));
            
            if (age != null)
                result = result.Where(p => p.Age >= age);
            
            return result;
        }
    }
}